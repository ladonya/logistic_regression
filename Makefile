package = dslr


all: install clean

install:
	pip3.6 install --user .

install_venv:
	pip3.6 install .

install_dev:
	pip3.6 install --editable .

isort:
	isort -rc $(package)

flake:
	flake8 $(package)

uninstall:
	pip3.6 uninstall $(package) -y
	pip3.6 uninstall -r requirements.txt -y

clean:
	rm -rf `find . -name __pycache__`
	rm -f `find . -type f -name '*.py[co]' `
	rm -f `find . -type f -name '*~' `
	rm -f `find . -type f -name '.*~' `
	rm -f `find . -type f -name '@*' `
	rm -f `find . -type f -name '#*#' `
	rm -f `find . -type f -name '*.orig' `
	rm -f `find . -type f -name '*.rej' `
	rm -f `find . -type f -name '*.egg-info' `
	rm -f .coverage
	rm -rf coverage
	rm -rf cover
	rm -rf htmlcov
	rm -rf .cache
	rm -rf .eggs
	rm -rf *.egg-info

.PHONY: all install install_dev isort flake uninstall clean
