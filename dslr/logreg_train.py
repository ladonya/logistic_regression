import json
import sys
from argparse import ArgumentParser
from copy import deepcopy

import pandas as pd

from . import const
from .common import get_best_prediction, h, normalize_input


def init_weights():
    return dict(map(lambda x: (x, 0), const.INDEPENDENT_VARS + ['bias', ]))


def is_accurate_enough(old_weights, new_weights, accuracy):
    for key in old_weights.keys():
        if abs(old_weights[key] - new_weights[key]) >= accuracy:
            return False
    return True


def update_weights(weights, data, lr, house_num):
    for i, row in data.iterrows():
        res = 0 if row[const.HOUSE_COL] != house_num else 1
        hipotesis = h(row.to_dict(), weights)
        for col_name in const.INDEPENDENT_VARS:
            weights[col_name] += (lr * (res - hipotesis)
                                  * hipotesis * (1 - hipotesis)
                                  * row[col_name])
        weights['bias'] += lr * (res - hipotesis)
    return weights


def train_for_house(data, lr, house_num, num_iter, accuracy):
    weights = init_weights()
    for i in range(num_iter):
        new_weights = update_weights(deepcopy(weights), data, lr, house_num)
        if is_accurate_enough(weights, new_weights, accuracy):
            break
        weights = new_weights
    return weights


def train(data, num_iter=25, lr=0.2, accuracy=0.1):
    weights = []
    for house_num in range(4):
        weights.append(train_for_house(data, lr, house_num, num_iter,
                                       accuracy))
    return weights


def save_weights(weights, filepath):
    with open(filepath, 'w') as fp:
        fp.write(json.dumps(weights))


def estimate_error(data, weights):
    guess = 0
    len_data = data.shape[0]
    for i, row in data.iterrows():
        if get_best_prediction(row.to_dict(), weights) == row[const.HOUSE_COL]:
            guess += 1
    return guess / len_data * 100


def parse_args(args, usage):
    parser = ArgumentParser(usage=usage)
    parser.add_argument('file_path',
                        type=str,
                        help='path to csv file')
    parser.add_argument('-o',
                        type=str,
                        help='output json file with weights',
                        default=const.SAVE_PATH)
    parser.add_argument('-n',
                        type=int,
                        help='number iterations',
                        default=25)
    parser.add_argument('-lr',
                        type=float,
                        help='learning rate',
                        default=0.2)
    parser.add_argument('-acc',
                        type=float,
                        help='accuracy',
                        default=0.1)
    args = parser.parse_args(args)
    return args.file_path, args.o, args.n, args.lr, args.acc


def logreg_train(args):
    try:
        file_path, output_file, n, lr, acc = parse_args(
            args,
            'dslr logreg_train [-h] [-o O] [-n N]'
            ' [-lr LR] [-acc ACC] file_path')
        data = pd.read_csv(
            file_path,
            skipinitialspace=True,
            usecols=const.INDEPENDENT_VARS + [const.HOUSE_COL, ])
        data = normalize_input(data, process_house=True)
        weights = train(data, num_iter=n, lr=lr, accuracy=acc)
        save_weights(weights, output_file)
        print('Model has been trained!')
        print('Accuracy: {:3.2f}%'.format(estimate_error(data, weights)))
        print(f'Weights has been saved to "{output_file}"')
    except Exception:
        print('Wrong input file. Exiting...')


if __name__ == '__main__':
    """
    dslr logreg_train [-h] [-o O] [-n N] [-lr LR] [-acc ACC] file_path
    """
    logreg_train(sys.argv[1:])
