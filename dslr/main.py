import sys
from argparse import ArgumentParser

from .describe import run as describe
from .histogram import histogram
from .logreg_predict import logreg_predict
from .logreg_train import logreg_train
from .pair_plot import pair_plot
from .scatter_plot import scatter_plot


class Dispatcher:
    def __init__(self):
        parser = ArgumentParser()
        parser.add_argument('command',
                            choices=['describe',
                                     'histogram',
                                     'scatter_plot',
                                     'pair_plot',
                                     'logreg_train',
                                     'logreg_predict'])
        command = parser.parse_args(sys.argv[1:2]).command
        getattr(self, command)(sys.argv[2:])

    @staticmethod
    def describe(args):
        describe(args)

    @staticmethod
    def histogram(args):
        histogram(args)

    @staticmethod
    def scatter_plot(args):
        scatter_plot(args)

    @staticmethod
    def pair_plot(args):
        pair_plot(args)

    @staticmethod
    def logreg_train(args):
        logreg_train(args)

    @staticmethod
    def logreg_predict(args):
        logreg_predict(args)


def main():
    Dispatcher()


if __name__ == '__main__':
    main()
