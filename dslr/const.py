from collections import OrderedDict

SAVE_PATH = 'weights.json'


HOUSES = OrderedDict([
    ('Gryffindor', 0),
    ('Slytherin', 1),
    ('Ravenclaw', 2),
    ('Hufflepuff', 3)
])


HANDS = {
    'Right': 0,
    'Left': 1
}


INDEPENDENT_VARS = [
    'Astronomy',
    'Herbology',
    'Divination',
    'Muggle Studies',
    'History of Magic',
    'Transfiguration',
    'Charms',
    'Flying',
]


HOUSE_COL = 'Hogwarts House'
