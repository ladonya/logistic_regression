import math
import sys
from argparse import ArgumentParser
from collections import OrderedDict
from copy import deepcopy
from functools import reduce
from math import sqrt

import pandas as pd
from numpy import isnan

from . import const


def describe(data):
    columns = list(data)
    columns_dict = OrderedDict(zip(columns, [0 for i in range(len(columns))]))
    count_result = count(data, columns_dict)
    mean_result = mean(data, columns_dict, count_result)
    min_result = min_max(data, columns_dict)
    max_result = min_max(data, columns_dict, find='max')
    std_result = std(data, columns_dict, mean_result, count_result)
    percentile_25 = percentile(data, 25, columns_dict)
    percentile_50 = percentile(data, 50, columns_dict)
    percentile_75 = percentile(data, 75, columns_dict)
    result = pd.DataFrame([count_result,
                           mean_result,
                           std_result,
                           min_result,
                           percentile_25,
                           percentile_50,
                           percentile_75,
                           max_result],
                          index=['count',
                                 'mean',
                                 'std',
                                 'min',
                                 '25%',
                                 '50%',
                                 '75%',
                                 'max'])
    return result


def _percentile(N, percent):
    if not N:
        return None
    k = (len(N) - 1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return N[int(k)]
    d0 = N[int(f)] * (c - k)
    d1 = N[int(c)] * (k - f)
    return d0 + d1


def count(data: pd.DataFrame, columns_dict):
    columns_dict = deepcopy(columns_dict)
    for i, row in data.iterrows():
        for key in columns_dict.keys():
            if not isnan(row[key]):
                columns_dict[key] += 1
    return columns_dict


def std(data, columns_dict, mean_result, count_result):
    columns_dict = deepcopy(columns_dict)
    for key in columns_dict.keys():
        clean_column = filter(lambda x: not isnan(x), data[key])
        squares = [(row - mean_result[key])**2 for row in clean_column]
        columns_dict[key] = sqrt(reduce(lambda x, y: x + y, squares)
                                 / (count_result[key] - 1))
    return columns_dict


def mean(data, columns_dict, count_results):
    columns_dict = deepcopy(columns_dict)
    for key in columns_dict.keys():
        clean_column = filter(lambda x: not isnan(x), data[key])
        columns_dict[key] = (reduce(lambda x, y: x + y, clean_column)
                             / count_results[key])
    return columns_dict


def min_max(data, columns_dict, find='min'):
    columns_dict = deepcopy(columns_dict)
    for key in columns_dict.keys():
        clean_column = filter(lambda x: not isnan(x), data[key])
        if find == 'min':
            columns_dict[key] = reduce(
                lambda x, y: x if x < y else y, clean_column)
        elif find == 'max':
            columns_dict[key] = reduce(
                lambda x, y: x if x > y else y, clean_column)
        else:
            raise AssertionError('"find" should be either "min" or "max"')
    return columns_dict


def percentile(data, percentage, columns_dict):
    columns_dict = deepcopy(columns_dict)
    for key in columns_dict.keys():
        clean_column = sorted(filter(lambda x: not isnan(x), data[key]))
        columns_dict[key] = _percentile(clean_column, percentage / 100)
    return columns_dict


def run(args):
    try:
        parser = ArgumentParser(usage='dslr describe [-h] file_path')
        parser.add_argument('file_path',
                            type=str,
                            help='path to csv file')
        file_path = parser.parse_args(args).file_path
        data = pd.read_csv(file_path)
        if data[const.HOUSE_COL].isnull().all():
            data = data.drop(columns=[const.HOUSE_COL])
        print(describe(data._get_numeric_data()))
    except Exception:
        print('Wrong input file. Exiting...')


if __name__ == '__main__':
    run(sys.argv[1:])
